#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  readXML.py
#
#  Copyright 2019 Geova <geova@geova-X555LA>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#


from xml.etree import ElementTree
import numpy as np
import cv2


def readXml(path):

    ######INTRINSICS MATRIX K1######## /home/disoji/Documents/Gits/GIT_Proyecto_DISEÑO/integrated_butterfly_tracking_tool/Triagulation_Module/CalibrationXML/camera0CalibrationParams.xml
    # parse an xml file by name /home/disoji/Documents/Gits/GIT_Proyecto_DISEÑO/integrated_butterfly_tracking_tool/Triagulation_Module/CalibrationXML
    path0 = path + "camera0CalibrationParams.xml"
    tree = ElementTree.parse(path0) #TODO: Redirect to final folder path (using environment variable) - COMPLETE
    olp = tree.findall(".//data")
    cam0 = [t.text for t in olp]
    x = cam0[0].split(" ")
    y = []
    for i in range(0, len(x)):
        if len(x[i]) > 1:
            y.append(x[i])
    x = []
    for i in range(0, len(y)):
        x.append(y[i].split("\n"))

    K1 = np.zeros((3, 3), np.float32)
    for i in range(0, 3):
        K1[0][i] = float(x[i][0])
    for i in range(0, 3):
        K1[1][i] = float(x[i+3][0])
    for i in range(0, 3):
        K1[2][i] = float(x[i+6][0])

 ######INTRINSICS MATRIX K2########
    # parse an xml file by name
    path1 = path + "camera1CalibrationParams.xml"
    tree = ElementTree.parse(path1) #TODO: Change to final path too - COMPLETE
    olp = tree.findall(".//data")
    cam0 = [t.text for t in olp]
    x = cam0[0].split(" ")
    y = []
    for i in range(0, len(x)):
        if len(x[i]) > 1:
            y.append(x[i])
    x = []
    for i in range(0, len(y)):
        x.append(y[i].split("\n"))

    K2 = np.zeros((3, 3), np.float32)
    for i in range(0, 3):
        K2[0][i] = float(x[i][0])
    for i in range(0, 3):
        K2[1][i] = float(x[i+3][0])
    for i in range(0, 3):
        K2[2][i] = float(x[i+6][0])

   # print(intrinsics)
   ######TRANSLATION MATRIX########
    path2 = path + "cameraCalibrationParams.xml"
    tree = ElementTree.parse(path2) #TODO: Change to final path too - COMPLETE
    olp = tree.findall(".//data")
    cam0 = [t.text for t in olp]
    x = cam0[0].split(" ")
    y = []
    for i in range(0, len(x)):
        if len(x[i]) > 1:
            y.append(x[i])
    t = np.zeros((3, 1), np.float32)
    for i in range(0, len(y)):
        t[i][0] = (float(y[i]))
    ######ROTATION MATRIX########
    x = cam0[1].split(" ")
    y = []
    for i in range(0, len(x)):
        if len(x[i]) > 1:
            y.append(x[i])
    RAux = np.zeros((3, 1), np.float32)
    for i in range(0, len(y)):
        RAux[i] = (float(y[i]))
    R = np.zeros((3, 3), np.float32)
    cv2.Rodrigues(RAux, R)
    #P1 = K1*[I | z]
    I = np.identity(3, dtype=float)
    z = np.zeros((3, 1), np.float32)
    P1 = np.concatenate((np.dot(K1, I), np.dot(K1, z)), axis=1)
    #P2 = K2*[R | t]
    P2 = np.concatenate((np.dot(K2, R), np.dot(K2, t)),
                        axis=1)  # print( np.transpose(dst))

    #print(P1)
    #print(P2)
    #print(P2[0][0])
    return P1,P2