# Multi-object multi-camera tracking tool.

## Summary

This project has been developed by students of Computer Engineering
and Electronics Engineering at SIPLab, Costa Rica Institute of
Tecnology.

It has been tailored to the particular case of butterfly tracking in
an enclosing box.  The scenario corresponds to a CICANUM project at
UCR.

## History

- v0.0   20.01.01 Initial version from Diego Solis' repository
- v0.1   20.01.21 Links updated

## Authors

- Cristian Castillo (2020)
- Esteban Sanabria (2020)
- Diego Solís (2019)
- Alfredo Piedra (2019)
- Geovanny Espinoza (2019)
- Oscar Ulate (2019)
- Alejandro Solís (2019-2020)
- David Peraza (2019-2020)
- Emmanuel Madrigal (2018)
- Mauricio Montero (2018)
- Abraham Arias (2018)


## How to install dependencies

The dependencies are automatically installed with the script `dependencies.sh`

    sh dependencies.sh


### Spinnaker 1.19 for Python (PySpin)

This project requires the proprietary drivers for the FLIR cameras
used in the project (Blackfly S BFS-U3-13Y3M-C) .  Hence, you need to
install *Spinnaker*:

Get the software at the [FLIR site](http://www.flir.com).  At the time
of writing this readme you can find [Spinnaker
here](https://flir.app.boxcn.net/v/SpinnakerSDK).  If it is not there
anymore, just search for Spinnaker SDK.

This is necessary only if you are using the industrial cameras.  If
you just need the webcam based version, this might not be necessary.

It is necessary to register to the FLIR site in order to download the
python distribution for your python version and architecture.  The
python version is nothing but a wrapper, hence you still need the
standard C version (in the site where you download the wheel file for
GNU/Linux there should be a folder called python and other files,
those files are the standard version and in the folder called python
there is the wrapper). The folder you download as a compressed file
contains a README for installation.

**Important**: Increase USB file system memory.  The default size of
memory buffers used by the USB drivers is simply unsufficient to cope
with the data volumes provided by three industrial cameras.  Hence, it
is absolutely necessary to increase that size, or the cameras will now
work properly or at all.

In `/etc/default/grub` locate the line (open it with your favorite
editor as sudo)

     GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"

In your particular case this line may may have other arguments.
Simply ensure that the las argument is as follows:

     GRUB_CMDLINE_LINUX_DEFAULT="quiet splash usbcore.usbfs_memory_mb=1000"

Then run:

     $ sudo update-grub2 (or installed grub version)

Update grub with these settings:

     $ sudo update-grub

Reboot and test a USB 3.1 camera.

If this method fails to set the memory limit, run the following command:

     $ sudo sh -c 'echo 1000 > /sys/module/usbcore/parameters/usbfs_memory_mb'

To confirm that you have successfully updated the memory limit, run
the following command:

     $ cat /sys/module/usbcore/parameters/usbfs_memory_mb

## How to access more documentation

