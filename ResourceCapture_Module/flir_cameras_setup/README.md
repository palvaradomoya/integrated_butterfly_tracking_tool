# FLIR Cameras Sync
Configuración de las cámaras para la captura sincronizada de imágenes.

Este README, así como la solución, fue iniciado por Óscar Ulate, y modificado por Alejandro Solís.

## Guía de  documentación

### Configuración del ambiente

1. Instalar el SDK the Spinnaker para C y luego para Python 3.6.
Dentro de las posibles instalaciones están el Spinnaker SDk, pero además manuales y otros componentes que pueden ser útiles.
[Centro de descargas] (https://www.flir.com/support/products/blackfly-s-usb3#Resources)

#### Inatalación
Para instalar el produtco:
1. Descargar la versión para Linux.
2. Abrir la carpeta de descarga y seguir las instrucciones para Linux (claro). *En lo personal me funcionó mucho mejor Ubuntu 18.04 que Ubuntu 16.04 para instalar los paquetes de Python. Esto porque me daba errores para  instalar los paquetes del whl con pip, aunque debería funcionar bien con ambos*.
En la carpeta de instalación viene los ejemplos para Python 3.
3. Es importantísimo seguir esta guia una vez instalado el producto para aumentar el espacio asignado a USBFS. Sin esto no va a poder ni siquiera comenzar a tomar fotos.
[Guia oficial](https://www.flir.com/support-center/iis/machine-vision/application-note/understanding-usbfs-on-linux/)
[La guia que yo seguí](https://importgeek.wordpress.com/2017/02/26/increase-usbfs-memory-limit-in-ubuntu/)

Abrir el SpinView que se instala automaticamente con el Spinnaker SDK. Si con ese software  puede tomar una imagen, significa que se instaló correctamente.

### Support centre
En este sitio, se encuentra la página principal para el soporte de las cámaras que se están usando.
De esta página se puede abrir un ticket para soporte  y buscar la documentación necesaria.

Es necesario tener en cuenta las características de las cámaras para encontrar la documentación necesaria:
* Model: Blackfly S BFS-U3-13Y3M
* Firmware: 1608.0.47.0
* Part Number: BFS-U3-13Y3M-C
* 1,3 MP, Black & White

** Hay muchas cámaras similares, pero no todas tienen los mismos features.

[Centro de soporte](https://www.flir.com/support/products/blackfly-s-usb3)

### Documentación importante
De entendimiento general:
[Manejo del buffer de la cámara](https://www.flir.com/support-center/iis/machine-vision/application-note/understanding-buffer-handling/)
	> FLIR recomienda asignar un buffer de 1000MB, o más si se trabaja con más de una cámara.
[GPIO de la cámara](http://softwareservices.ptgrey.com/BFS-U3-13Y3/latest/Family/GPIOConnector.htm?Highlight=gpio)

Estos dos se usan directamente con el trigger. Son importantes:
[Acquisition control](http://softwareservices.ptgrey.com/BFS-U3-13Y3/latest/Model/public/AcquisitionControl.html#TriggerMode)
[Device control](http://softwareservices.ptgrey.com/BFS-U3-13Y3/latest/Model/public/DeviceControl.html#TimestampLatch)

Manual técnico de la cámara: (Capítulo 7 me  parece imprescindible)
[Manual técnico](https://flir.app.boxcn.net/s/jw17hga6i36z7cfifd6l0rw3jma9ow9g/file/418588576484)

Manual de configuración del trigger (Toda la magia):
[Control del trigger](https://www.flir.com/support-center/iis/machine-vision/application-note/configuring-synchronized-capture-with-multiple-cameras/)
