#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
from PyQt5.QtWidgets import QApplication

from CalibrationMain_Module.src.MainWindow import MainWindow
from CalibrationMain_Module.src.AppCore import AppCore


def start():
    coreLogic = AppCore()
    app = QApplication(sys.argv)
    #app.setStyle('Breeze')
    w = MainWindow(coreLogic)
    return app.exec_()


if __name__ == "__main__":

    # here argparse your CLI arguments, to choose
    # interface (readline, ncurses, Qt, web, whatever...?)
    # and setup the application (logfile, port to bind to, look
    # of the GUI...)
    sys.exit(start())
