# -*- coding: utf-8 -*-
"""
Butterflies application - SIP-Lab.

CalibrationAlgorithm.py
Created: 2018/09/01
Author: Abraham Arias Chinchilla

This module performs the intrinsic and extrinsic calibration
for the cameras

Todo:
    *Correct both index to iterate over cameras, =3
"""

import cv2
import numpy as np
import glob
import CalibrationMain_Module.src.utils.Utils as utils
import datetime

class CalibrationAlgorithm:
    """This .

    The __init__ method may be documented in either the class level
    docstring, or as a docstring on the __init__ method itself.

    Either form is acceptable, but the two should not be mixed. Choose one
    convention to document the __init__ method and be consistent with it.

    Note:
        Do not include the `self` parameter in the ``Args`` section.

    Args:
        msg (str): Human readable string describing the exception.
        code (:obj:`int`, optional): Error code.

    Attributes:
        msg (str): Human readable string describing the exception.
        code (int): Exception error code.

    """

    def __init__(self, settings): 
        lenght=10
        self.axis = np.float32([[0,0,0], [lenght,0,0], [0,lenght,0], [0,0,-lenght]]).reshape(-1,3)
        self.criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 3, 0.01)
        self.criteriaHard = stop_criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
        self.IgnoreVAL = np.nan
        self.sets = settings
        self.borderSize=12
        self.lineColor = (255,0,0)

    def findChessboardsFast(self, imgs):
        """To comment.
        Args:
            A (int): A.

        Returns:
            bool: R.

        """ 
        patternsFound = [0,0,0]
        for i in range(0,len(imgs)-1):
            #gray = cv2.cvtColor(imgs[i],cv2.COLOR_BGR2GRAY)
            try:
                ret, corners = cv2.findChessboardCorners(imgs[i], self.sets.patternSize,None, cv2.CALIB_CB_FAST_CHECK)
                height,width = imgs[i].shape[:2]
                if ret:
                    imgs[i] = cv2.rectangle(imgs[i], (0,0), (width-1,height-1), (0,255,0), self.borderSize)
                    patternsFound[i]=1
                else:
                    imgs[i] = cv2.rectangle(imgs[i], (0,0), (width-1,height-1), (255,0,0), self.borderSize)
            except:
                print("Camera disconnected")
        return patternsFound
    
    #Calibration for intrinsic and extrinsic parameters from all cameras
    #def Calibration(self, progress_callback,calName):
    def Calibration(self, progress_callback):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        #Set dateTime as calibration name
        #calName[0] = str(datetime.datetime.now())
        #Run intrinsic calibration algorithm
        #e = self.intrinsic_cal(progress_callback,calName)
        e = self.intrinsic_cal(progress_callback)
        progress_callback.emit(50)
        #Run extrinsic calibration algorithm
        #e2 = self.extrinsic_cal(progress_callback,calName)
        e2 = self.extrinsic_cal(progress_callback)
        progress_callback.emit(100)

        return 1
        #return e and e2

    #def intrinsic_cal(self, progress_callback,calName):
    def intrinsic_cal(self, progress_callback):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """

        for cam in range(0,3):#For each camera
            workDone = 0
            
            imgs=[]
            #folder_name = "exp_file/calibraciones/" + calName[0] + "/imgs_calib/int_img/"
            #images_name = glob.glob("exp_file/calibraciones/" + calName[0] + "/imgs_calib/int_img/calib_cam" + str(cam) + "_img*[0-99].png")

            images_name = glob.glob("./CalibrationMain_Module/data/int_img/calib_cam"+str(cam)+"_img*[0-99].png") #TODO: Change path to save folder here

            for fname in images_name: #For each image of each camera
                imgs.append(cv2.imread(fname))
            ret, mtx, dist, rvecs, tvecs = self.calibrateIntrinsicChessboards(cam, imgs)
            self.sets.saveCamerasIntrinsicParams(cam, mtx,dist)

        return 0

    def calibrateIntrinsicChessboards(self, camID, imgs):

        # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)        
        objp = utils.createObjPoints(self.sets.patternSize, 
                                         self.sets.patternHeightSize,
                                         self.sets.patternWithSize)

        # Arrays to store object points and image points from all the images.
        objpoints = [] # 3d point in real world space
        imgpoints = [] # 2d points in image plane.
        gray = []
        for fname in imgs: #For each image of each camera
            #img = cv2.imread(fname)
            gray = cv2.cvtColor(fname, cv2.COLOR_BGR2GRAY)
            # Find the chess board corners
            ret, corners = cv2.findChessboardCorners(image=gray,
                                                     patternSize=self.sets.patternSize,
                                                     flags=0)
            # If found, add object points, image points (after refining them)
            if ret:
                objpoints.append(objp)
                corners = cv2.cornerSubPix(image=gray,
                                           corners=corners, 
                                           winSize=(11,11), 
                                           zeroZone=(-1,-1), 
                                           criteria=self.criteriaHard)
                imgpoints.append(corners)
        if not len(objpoints) == 0:

            k = cv2.initCameraMatrix2D(objectPoints=objpoints,
                                       imagePoints=imgpoints, 
                                       imageSize=gray.shape[::-1], 
                                       aspectRatio=1)

            distI = np.zeros((8,1))
            ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objectPoints=objpoints, 
                                                                imagePoints=imgpoints, 
                                                                imageSize=gray.shape[::-1], 
                                                                cameraMatrix=k, 
                                                                distCoeffs=distI,
                                                                flags=0)
                                                                #cv2.CALIB_USE_INTRINSIC_GUESS)
                                                                #|
                                                                #cv2.CALIB_RATIONAL_MODEL)
                                                                #cv2.CALIB_THIN_PRISM_MODEL|
                                                                #cv2.CALIB_TILTED_MODEL)
            # compute Reprojection Errors
            err = utils.calcError(imgpoints, objpoints, rvecs, tvecs, mtx, dist)
            print( "Cam"+str(camID)+" total error: "+str(err) )


        else:
            ret = mtx = dist = rvecs = tvecs = []
        return [ret, mtx, dist, rvecs, tvecs]


    #def extrinsic_cal(self, progress_callback,calName):
    def extrinsic_cal(self, progress_callback):
        """To comment.

        Create an index to link pairs of images
         with the same pattern. Its a matrix like:
         [ [link_0_1]
          [link_0_2]
          [link_1_2] ]
         1:There is a pattern in both cameras
         0: No pattern in pair of cameras.
         The number of the column indicates the image number.

        Args:
            A (int): A.

        Returns:
            bool: R.

    """ 
        pairsTVecs = [[0],[0],[0]]
        pairsRVecs = [[0],[0],[0]]
        #files_sub = "exp_file/calibraciones/" + calName[0] + "/imgs_calib/ext_img/calib_cam"
        files_sub = "./CalibrationMain_Module/data/ext_img/calib_cam"
        #For each camera
        for i in range(0,3):
            # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
            #objp = np.zeros((self.sets.patternSize[0]*self.sets.patternSize[1],3), np.float32)
            #objp[:,:2] = np.mgrid[0:(self.sets.patternSize[0]),0:self.sets.patternSize[1]].T.reshape(-1,2)
            objp = utils.createObjPoints(self.sets.patternSize, 
                                         self.sets.patternHeightSize,
                                         self.sets.patternWithSize)

            # Arrays to store object points and image points from all the images.
            objpoints = [] # 3d point in real world space
            imgpoints = [] # 2d points in image plane.
            print("Whaat is going on?")
            images = glob.glob(files_sub+str(i)+"_img[0-9].png") # _img*[0-99].png

            imgs_found = np.zeros((len(images)))
            for j, fname in enumerate(sorted(images)): #For each image of each camera
                #Fill the link index
                img = cv2.imread(fname)
                gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                # Find the chess board corners
                ret, corners = cv2.findChessboardCorners(image=gray, 
                                                        patternSize=(self.sets.patternSize[0],self.sets.patternSize[1]), 
                                                        flags=cv2.CALIB_CB_ADAPTIVE_THRESH+cv2.CALIB_CB_NORMALIZE_IMAGE)
                # If found, add object points, image points (after refining them)
                if ret:
                    imgs_found[j] = 1
                    objpoints.append(objp)
                    corners2 = cv2.cornerSubPix(image=gray,
                                                corners=corners, 
                                                winSize=(11,11), 
                                                zeroZone=(-1,-1), 
                                                criteria=self.criteria)
                    imgpoints.append(corners)
            if len(objpoints)>0:
                ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objectPoints=objpoints, 
                                                                   imagePoints=imgpoints, 
                                                                   imageSize=gray.shape[::-1], 
                                                                   cameraMatrix=None, 
                                                                   distCoeffs=None,
                                                                   flags=0)
                #'''
                complt_tvecs = np.zeros((len(images),3,1))
                complt_rvecs = np.zeros((len(images),3,1))
                vec_i=0
                for k in range(0, len(images)):
                    if imgs_found[k]==1:
                        complt_tvecs[k] = tvecs[vec_i]
                        complt_rvecs[k] = rvecs[vec_i]
                        vec_i+=1
                pairsTVecs[i] = complt_tvecs
                pairsRVecs[i] = complt_rvecs
                #'''
                #pairsTVecs[i] = np.asarray(tvecs)
                #pairsRVecs[i] = np.asarray(rvecs)

            else:
                zeros_pair = np.zeros((len(images),3,1))
                pairsRVecs[i] = zeros_pair
                pairsTVecs[i] = zeros_pair

        R_mean, T_mean = self.calculateCoords(pairsTVecs, pairsRVecs)

        #R_mean[2], T_mean[2] = self.guessExtrinsic(R_mean, T_mean)

        #print("Rotation:")
        #print(R_mean)
        print("Translation:")
        print(T_mean)

        self.sets.saveCamerasExtrinsicParams(T_mean, R_mean)
        return 1

    def calculateCoords(self, mtxT, mtxR):

        camCoord_R, camCoord_T = self.calculateCamPos(mtxT, mtxR)
        
        #print(camCoord_R)
        #print(camCoord_T)

        cams_Rotate = utils.promediateCols(camCoord_R)
        cams_Translate = utils.promediateCols(camCoord_T)

        return [cams_Rotate, cams_Translate]

    def calculateCamPos(self, mtxT, mtxR):
        imgPairs = [(0,1), (1,2), (0,2)]
        #Find coordenates to transform points from 
        #one camera to another one
        camCoord_R = np.zeros( (3, len(mtxR[0]), 3) ) #TODO; might be necessary to add dtype=int
        camCoord_T = np.zeros( (3, len(mtxT[0]), 3) )
        
        for i, pair in enumerate(imgPairs):#For each pair of cameras
            for j in range(0, len(mtxT[0])): #For each image in cameras
                if( sum(mtxT[pair[0]][j])==0 or sum(mtxR[pair[0]][j])==0 or
                    sum(mtxT[pair[1]][j])==0 or sum(mtxR[pair[1]][j])==0 ):
                    camCoord_R[i][j] = self.IgnoreVAL
                    camCoord_T[i][j] = self.IgnoreVAL
                else:
                    camCoord_R[i][j], camCoord_T[i][j] = self.transformFromCamToCam(mtxR[pair[0]][j],
                                                                                    mtxT[pair[0]][j],
                                                                                    mtxR[pair[1]][j],
                                                                                    mtxT[pair[1]][j])
        return [camCoord_R, camCoord_T]

    def transformFromCamToCam(self, Ra, Ta, Rb, Tb):
        R_1,_ = cv2.Rodrigues(Ra)
        T_1 = Ta

        R_2,_ = cv2.Rodrigues(Rb)
        T_2 = Tb

        #Rotation = R1 * R2.t
        R1_R2t = np.dot(R_1, np.transpose(R_2))
        #Translation = -(R1 * R2.t * T2) + T1
        R1_R2t_T2 = np.dot( R1_R2t , T_2)
        _R1_R2t_T2_plus_T1 = np.add( -R1_R2t_T2, T_1)

        rot_V,_ = cv2.Rodrigues(R1_R2t)
        tra_V = _R1_R2t_T2_plus_T1

        return [np.transpose(rot_V),np.transpose(tra_V)]

    '''
    def guessExtrinsic(self, R_mean, T_mean):
        print(T_mean)

        r_mean_v,_ = cv2.Rodrigues(R_mean[0])
        Q2_in_C0 = utils.map(T_mean[1], r_mean_v, T_mean[0])

        print(Q2_in_C0)
        return [[0,0,0],[0,0,0]]
    '''
    def drawAndExtrinsicGuess(self, imgs, camIDsel):
        '''
        Find points in one camera, transform them into 
        the coordinate system of another camera and draw them

        imgs= [cam0, cam1, cam2, camInstruction]: array with each 
        input frame of the cameras. And a fourth one to draw the result
        of the selected camera.
        '''
        error = 0
        guessFrom = 1
        guessFrom2 = 2
        camIDsel = 0
        objp = utils.createObjPoints(self.sets.patternSize, 
                                     self.sets.patternHeightSize,
                                     self.sets.patternWithSize)
        #if (camIDsel == 1):
        #    guessFrom = 0

        #FIND POINTS IN CAM1
        gray = cv2.cvtColor(imgs[guessFrom].copy(), cv2.COLOR_BGR2GRAY)
        ret, corners = cv2.findChessboardCorners(image=gray,
                                                 patternSize=self.sets.patternSize,
                                                 flags=cv2.CALIB_CB_FAST_CHECK)
        if ret:

            ret,rvecs, tvecs = cv2.solvePnP(objp,
                                            corners,
                                            self.sets.m[guessFrom],
                                            self.sets.d[guessFrom])

            # Transform points to coordinate system of camera 1
            rvecsM,_ = cv2.Rodrigues(rvecs)
            Q1 = utils.map(self.axis,
                           rvecsM,
                           tvecs)

            # Transform points to coordinate system of camera 0
            mrvecs,_ = cv2.Rodrigues(self.sets.extrinsic_Rotation[0])
            Q0 = utils.map(Q1,
                           mrvecs,
                           self.sets.extrinsic_Translation[0].reshape(3,1))

            # Project points in camera 0
            imgpts,_ = cv2.projectPoints(objectPoints=Q0,
                                         rvec=np.zeros( (3,1), np.float32),
                                         tvec=np.zeros( (3,1), np.float32),
                                         cameraMatrix=self.sets.m[camIDsel],
                                         distCoeffs=self.sets.d[camIDsel])
            #PROJECTED axis
            utils.drawAxis(imgs[3], imgpts.astype(int))

        #FIND POINTS IN CAM2
        gray2 = cv2.cvtColor(imgs[guessFrom2].copy(), cv2.COLOR_BGR2GRAY)
        ret2, corners2 = cv2.findChessboardCorners(image=gray2,
                                                 patternSize=self.sets.patternSize,
                                                 flags=cv2.CALIB_CB_FAST_CHECK)
        if ret2:

            objp2 = utils.createObjPoints(self.sets.patternSize, 
                                         self.sets.patternHeightSize,
                                         self.sets.patternWithSize)
            ret2,rvecs2, tvecs2 = cv2.solvePnP(objp2,
                                            corners2,
                                            self.sets.m[guessFrom2],
                                            self.sets.d[guessFrom2])

            # Transform points to coordinate system of camera 2
            rvecsM2,_ = cv2.Rodrigues(rvecs2)
            Q2 = utils.map(self.axis,
                           rvecsM2,
                           tvecs2)

            # Transform points to coordinate system of camera 1
            mrvecs2,_ = cv2.Rodrigues(self.sets.extrinsic_Rotation[1])
            Q1P = utils.map(Q2,
                            mrvecs2,
                            self.sets.extrinsic_Translation[1].reshape(3,1))

            # Transform points to coordinate system of camera 0
            mrvecs22,_ = cv2.Rodrigues(self.sets.extrinsic_Rotation[0])
            Q0P = utils.map(Q1P,
                            mrvecs22,
                            self.sets.extrinsic_Translation[0].reshape(3,1))

            # Project points in camera 0
            imgpts2,_ = cv2.projectPoints(objectPoints=Q0P,
                                         rvec=np.zeros( (3,1), np.float32),
                                         tvec=np.zeros( (3,1), np.float32),
                                         cameraMatrix=self.sets.m[camIDsel],
                                         distCoeffs=self.sets.d[camIDsel])

            #PROJECTED axis
            utils.drawAxis(imgs[3], imgpts2.astype(int))

        #Find the true axis
        gray_orig = cv2.cvtColor(imgs[camIDsel].copy(), cv2.COLOR_BGR2GRAY)
        ret_orig, corners_orig = cv2.findChessboardCorners(image=gray_orig,
                                                           patternSize=self.sets.patternSize,
                                                           flags=cv2.CALIB_CB_FAST_CHECK)
        if ret_orig:
            ret_orig, rvec_orig, tvec_orig = cv2.solvePnP(objectPoints=objp,
                                                          imagePoints=corners_orig,
                                                          cameraMatrix=self.sets.m[camIDsel],
                                                          distCoeffs=self.sets.d[camIDsel])
            imgpts_orig, _ = cv2.projectPoints(objectPoints=self.axis,
                                               rvec=rvec_orig,
                                               tvec=tvec_orig,
                                               cameraMatrix=self.sets.m[camIDsel],
                                               distCoeffs=self.sets.d[camIDsel])
            #Distance from 2D points
            if ret:
                error = cv2.norm(np.subtract(imgpts[0],imgpts_orig[0]))
            
            #Line from the two axis, to compare
            #cv2.line(imgs[3], tuple(imgpts_orig[0].ravel()), tuple(imgpts[0].astype(int).ravel()), (255,255,255), 5)

            #REAL AXIS
            utils.drawAxis(imgs[3], imgpts_orig)

            '''
            TEST projections
            '''
            # Project points in camera 1
            '''
            imgpts_test,_ = cv2.projectPoints(objectPoints=Q1,
                                         rvec=np.zeros( (3,1), np.float32),
                                         tvec=np.zeros( (3,1), np.float32),
                                         cameraMatrix=self.sets.m[guessFrom],
                                         distCoeffs=self.sets.d[guessFrom])
            '''
            #utils.drawAxis(imgs[guessFrom], imgpts_test.astype(int))


        return error

    def drawChessboard(self, imgs, flagFound, cameraIDselected, calibration_done, showExtCalib):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """ 
        img = imgs[cameraIDselected].copy()
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        error = 0
        if not calibration_done:
            # If found, add object points, image points
            height,width = img.shape[:2]

            if sum(flagFound) >= 1:
                ret, corners = cv2.findChessboardCorners(gray, self.sets.patternSize,None, cv2.CALIB_CB_ADAPTIVE_THRESH+cv2.CALIB_CB_NORMALIZE_IMAGE)
                cv2.drawChessboardCorners(img, self.sets.patternSize, corners, ret)
                cv2.rectangle(img,(0,0),(width-1,height-1),(0,255,0),self.borderSize)

            else:#No pattern found
                cv2.rectangle(img,(0,0),(width-1,height-1),self.lineColor,self.borderSize)
                cv2.putText(img, "Pattern Not found", (15,35), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,0,0), 2, cv2.LINE_AA, False)
            imgs[3] = img
            return [imgs, error]
        else:
            if showExtCalib and sum(flagFound) >= 1:
                error = self.drawAndExtrinsicGuess(imgs, cameraIDselected)

            else:
                #undistort
                imgs[3] = cv2.undistort(img,
                                        self.sets.getCameraMatrix(cameraIDselected),
                                        self.sets.getCameraDistorsion(cameraIDselected))

            return [imgs, error]