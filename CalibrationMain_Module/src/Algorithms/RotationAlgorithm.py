import cv2.aruco as aruco
import numpy as np
import cv2

from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import (QApplication, QHBoxLayout, QOpenGLWidget, QSlider,QWidget)

class RotationAlgorithm(QOpenGLWidget):
	def __init__(self, setts):
		self.settings = setts
		self.y_center = self.settings.arUcoApos[0][1]
		self.x_center = self.settings.arUcoApos[2][0]
		self.aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
		self.parameters = aruco.DetectorParameters_create()

		self.actualInstruction = "none"
		self.previuosInstruction = "none"

	def drawMarkersInstructions(self,img, cameraIDselected):
		
		#lists of ids and the corners beloning to each id
		corners, ids, rejectedImgPoints = aruco.detectMarkers(cv2.cvtColor(img,cv2.COLOR_BGR2GRAY), self.aruco_dict, parameters=self.parameters)
		inst = 0
		if np.all(ids != None):
			rvec, tvec,_ = aruco.estimatePoseSingleMarkers(corners[0], 0.03, self.settings.getCameraMatrix(cameraIDselected), self.settings.getCameraDistorsion(cameraIDselected))

			img, inst = self.findMarkers(img,corners, rvec[0], tvec[0], cameraIDselected)
			#img = cv2.flip(img, 1)
			#aruco.drawAxis(img, self.settings.mtx, self.settings.dist, rvec[0], tvec[0], 0.06) #Draw Axis
			#aruco.drawDetectedMarkers(img, corners) #Draw A square around the markers
			#cv2.putText(img, "Id: " + str(ids), (0,64), self.settings.font, 1, (0,255,0),2,cv2.LINE_AA)
		return [img, inst]

	def findMarkers(self, img, corners, rvec, tvec, cameraID):
		axisPoints = np.float32([[0,0,0],[0.03,0,0],[0,0.03,0],[0,0,0.03]]).reshape(-1,3)
		imagePoints, jac = cv2.projectPoints(axisPoints, rvec, tvec, self.settings.getCameraMatrix(cameraID), self.settings.getCameraDistorsion(cameraID))
		
		markerInstruction = self.checkMarkerPosition(imagePoints)
		if(markerInstruction):
			height,width = img.shape[:2]
			cv2.rectangle(img,(0,0),(width-1,height-1),(0,255,0),8)
		else:
			img = cv2.line(img, tuple((imagePoints[0]).ravel()), tuple(imagePoints[1].ravel()), (255,0,0), 4)
			img = cv2.line(img, tuple(imagePoints[0].ravel()), tuple(imagePoints[2].ravel()), (0,0,255), 4)
			#img = cv2.line(img, tuple(imagePoints[0].ravel()), tuple(imagePoints[3].ravel()), (0,0,255), 5)
		inst = self.findInstructions(img,imagePoints)
		img = cv2.line(img, tuple(self.settings.arUcoApos[0]), tuple(self.settings.arUcoApos[1]), (0,255), 3) #Horizontal
		img = cv2.line(img, tuple(self.settings.arUcoApos[2]), tuple(self.settings.arUcoApos[3]), (0,255), 3) #Vertical	
		return [img, inst]


	def checkMarkerPosition(self, imagePoints):
		#[[150,150], [230,150], [190,110], [190,190]]
		in_x = imagePoints[0][0][0]
		in_y = imagePoints[0][0][1]
		in_x_top = imagePoints[1][0][0]
		in_y_top = imagePoints[1][0][1]

		if((abs(self.y_center-in_y)<self.settings.tolerance ) and (abs(self.x_center-in_x)<self.settings.tolerance) and
			(abs(self.y_center-in_y_top)<self.settings.tolerance) and (abs(self.x_center-40-in_x_top)<self.settings.tolerance)):#Center point and left
			return True
		else:
			return False

	def findInstructions(self, img,imagePoints):
		in_x = imagePoints[0][0][0]
		in_y = imagePoints[0][0][1]
		in_x_top = imagePoints[1][0][0]
		in_y_top = imagePoints[1][0][1]

		#Check for X rotation
		x_rotation = self.x_center-in_x
		y_rotation = self.y_center-in_y
		in_x_rotation_left = self.x_center-40-in_x_top
		in_y_rotation_left = self.y_center-in_y_top
		if( abs(x_rotation) > self.settings.tolerance ):#Need this rotation
			if (x_rotation>0):#To the left
				newInstruction = "RotateLeft"
			elif (x_rotation<0):#To the right
				newInstruction = "RotateRight"
		elif( abs(y_rotation) > self.settings.tolerance ):#Need this rotation
			if (y_rotation>0):#Rotate up
				newInstruction = "RotateUp"
			elif (y_rotation<0):#Rotate down
				newInstruction = "RotateDown"
		elif( abs(in_x_rotation_left)>self.settings.tolerance or abs(in_y_rotation_left)>self.settings.tolerance ):
			newInstruction = "Rotateclockwise"
		else:
			newInstruction = "none"
		self.updateState(newInstruction)
		return self.actualInstruction
		#if():#Check for Y rotation
		#if():#Check for Z rotation
		#if():#Check for distance

	def updateState(self, newInstruction):
		if(self.actualInstruction==newInstruction):
			pass
		else:
			self.actualInstruction=newInstruction
			print(self.actualInstruction)

