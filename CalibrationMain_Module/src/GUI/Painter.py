import sys
import math

from PyQt5.QtCore import pyqtSignal, QPoint, QSize, Qt
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import (QApplication, QHBoxLayout, QOpenGLWidget, QSlider,QWidget)
from PyQt5.Qt import QMutex

#from OpenGL.GLUT import *
#from OpenGL.GLU import *

import OpenGL.GL as gl
from OpenGL.GL import GL_ARRAY_BUFFER
import numpy as np
from pyassimp import load
import time

class Painter(QOpenGLWidget):
	xRotationChanged = pyqtSignal(int)
	yRotationChanged = pyqtSignal(int)
	zRotationChanged = pyqtSignal(int)

	def __init__(self, parent=None):

		self.mutex = QMutex()
		self.mutex.lock()
		super(Painter, self).__init__(parent)

		self.object = 0
		self.instructionObj = 0
		self.xRot = 0
		self.yRot = 0
		self.zRot = 0

		self.lastPos = QPoint()

		self.trolltechGreen = QColor.fromCmykF(0.40, 0.0, 1.0, 0.0)
		self.trolltechPurple = QColor.fromCmykF(0.39, 0.39, 0.0, 0.0)
		self.mutex.unlock()

	def getOpenglInfo(self):
		info = """
		    Vendor: {0}
		    Renderer: {1}
		    OpenGL Version: {2}
		    Shader Version: {3}
		""".format(
		    gl.glGetString(gl.GL_VENDOR),
		    gl.glGetString(gl.GL_RENDERER),
		    gl.glGetString(gl.GL_VERSION),
		    gl.glGetString(gl.GL_SHADING_LANGUAGE_VERSION)
		)

		return info

	def minimumSizeHint(self):
		return QSize(50, 50)

	def sizeHint(self):
		return QSize(400, 400)

	def setXRotation(self, angle):
	    angle = self.normalizeAngle(angle)
	    if angle != self.xRot:
	        self.xRot = angle
	        self.xRotationChanged.emit(angle)
	        self.update()

	def setYRotation(self, angle):
	    angle = self.normalizeAngle(angle)
	    if angle != self.yRot:
	        self.yRot = angle
	        self.yRotationChanged.emit(angle)
	        self.update()

	def setZRotation(self, angle):
		angle = self.normalizeAngle(angle)
		if angle != self.zRot:
			self.zRot = angle
			self.zRotationChanged.emit(angle)
			self.update()

	def initializeGL(self):
		self.mutex.lock()
		print(self.getOpenglInfo())

		self.setClearColor(self.trolltechPurple.darker())
		#self.object = self.makeObject()
		self.object = self.makeCameraModel()
		self.instructionObj = self.makeInsObj()
		gl.glShadeModel(gl.GL_FLAT)
		gl.glEnable(gl.GL_DEPTH_TEST)
		gl.glEnable(gl.GL_CULL_FACE)

		gl.glEnable(gl.GL_LIGHT0)
		flashLightPos = [ 0.0, 0.0, 0.0]
		gl.glLightfv(gl.GL_LIGHT0, gl.GL_POSITION, flashLightPos)
		self.mutex.unlock()

	def paintGL(self):
		self.mutex.lock()
		gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
		gl.glLoadIdentity()
		gl.glTranslated(0.0, 0.0, -10.0)
		gl.glRotated(self.xRot / 16.0, 1.0, 0.0, 0.0)
		gl.glRotated(self.yRot / 16.0, 0.0, 1.0, 0.0)
		gl.glRotated(self.zRot / 16.0, 0.0, 0.0, 1.0)
		gl.glCallList(self.object)
		gl.glCallList(self.instructionObj)
		self.mutex.unlock()

	def resizeGL(self, width, height):

		side = min(width, height)
		if side < 0:
			return

		gl.glViewport((width - side) // 2, (height - side) // 2, side,
		                   side)

		gl.glMatrixMode(gl.GL_PROJECTION)
		gl.glLoadIdentity()
		gl.glOrtho(-0.5, +0.5, +0.5, -0.5, 4.0, 15.0)
		gl.glMatrixMode(gl.GL_MODELVIEW)

	def mousePressEvent(self, event):
		self.lastPos = event.pos()

	def mouseMoveEvent(self, event):
		self.mutex.lock()
		dx = event.x() - self.lastPos.x()
		dy = event.y() - self.lastPos.y()

		if event.buttons() & Qt.LeftButton:
			self.setXRotation(self.xRot + 8 * dy)
			self.setYRotation(self.yRot + 8 * dx)
		elif event.buttons() & Qt.RightButton:
			self.setXRotation(self.xRot + 8 * dy)
			self.setZRotation(self.zRot + 8 * dx)

		self.lastPos = event.pos()
		self.mutex.unlock()

	def moveSomething(self, inst):
		# mutex lock con atributo de clase
		self.mutex.lock()
		if inst == 0 or inst == "none":
			self.instructionObj = self.makeInsObj()
		else:
			self.instructionObj = self.makeInsObj2()
		self.mutex.unlock()

	def makeCameraModel(self):
		#self.mutex.lock()
		#TODO: Cange this absolute path to something sane
		scene = load("/home/disoji/Documents/Gits/Proyecto_Diseño_Modules/proyecto_diseno_disoji/butterflies/icons/Pinhole_Camera/camera_tx.obj")
		scale = 120
		self.setColor(self.trolltechGreen)
		genList = gl.glGenLists(1)
		gl.glNewList(genList, gl.GL_COMPILE)
		gl.glBegin(gl.GL_TRIANGLES)

		
		for i, mesh in enumerate(scene.meshes):
			for j, face in enumerate(mesh.faces):
				vertexIndex = mesh.faces[j]
				textureIndex = mesh.faces[j]

				v = mesh.vertices[vertexIndex]
				#vt = mesh.texturecoords[textureIndex]
				
				'''
				if i == 0 and j ==0:
					print("Size of the faces: "+str(len(mesh.faces))+"x "+ str(len(mesh.faces[0])))
					print(mesh.faces[j])
					print(v)
					print("Textures: "+str(len(scene.textures)))
					print("Materials: "+str(len(scene.materials)))
					print("Mat[0]"+str(scene.materials[0].properties.items()))
					print("Mat index"+str(mesh.materialindex))
					print("")
				'''
				#gl.glMaterialfv(gl.GL_FRONT_AND_BACK, gl.GL_DIFFUSE, (0.0, 0.0, 0.0, 1.0));
				for k in range(0,len(v)):
					gl.glVertex3d(v[k][0]/scale,v[k][1]/scale,v[k][2]/scale)
					
		'''
		i = 1
		for index, face in enumerate(scene.meshes[i].faces):
			#print("Index:"+str(index))
			vertexIndex = scene.meshes[i].faces[index]
			#normalsIndex = scene.meshes[0].faces[1]
			#textureIndex = scene.meshes[0].faces[2]
			v = scene.meshes[i].vertices[vertexIndex]
			#vn = scene.meshes[0].vertices[normalsIndex]
			#vt = scene.meshes[0].vertices[textureIndex]
			for i in range(0,len(v)):
				#print("Vertex:"+str(v[i]))
				gl.glVertex3d(v[i][0]/scale,v[i][1]/scale,v[i][2]/scale)
				#print("---------")
		'''

		gl.glEnd()
		gl.glEndList()
		#self.mutex.unlock()
		return genList
		#return self.makeObject()

	def makeInsObj2(self):
		self.mutex.lock()
		genList = gl.glGenLists(1)
		gl.glNewList(genList, gl.GL_COMPILE)

		gl.glBegin(gl.GL_QUADS)  # GL_TRIANGLES
		#gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
				
		z_min = -0.1
		z_max = -0.2
		x = -3
		y = -3
		r = 0.4
		start_angle = 0
		i = start_angle
		end_angle = 90
		angle_increment = 0.1

		while(i < end_angle):
			x = r * math.cos(i*2*math.pi/360)
			y = r * math.sin(i*2*math.pi/360)

			gl.glVertex3d(x, y, z_min)
			gl.glVertex3d(x-0.05, y, z_min)
			gl.glVertex3d(x, y-0.05, z_min)
			gl.glVertex3d(x-0.05, y-0.05, z_min)
			#self.extrude(x,y,x-0.05,y-0.05)
			i += angle_increment
		'''
		#gluPartialDisk(gluNewQuadric(), 0.2, 0.25, 20, 30, 0, 360)
		'''

		gl.glVertex3d(0, 0.4+0.1, z_min)
		gl.glVertex3d(0, 0.25, z_min)
		gl.glVertex3d(-0.2, 0.4, z_min)
		gl.glVertex3d(-0.2, 0.25, z_min)

		gl.glEnd()
		gl.glEndList()
		return genList

	def makeInsObj(self):
		genList = gl.glGenLists(1)
		gl.glNewList(genList, gl.GL_COMPILE)

		gl.glBegin(gl.GL_QUADS)
		'''
		x1 = +0
		y1 = -0.08

		x2 = +0.08
		y2 = -0

		self.quad(x1, y1, x2, y2, y2, x2, y1, x1)

		self.extrude(x1, y1, x2, y2)
		self.extrude(x2, y2, y2, x2)
		self.extrude(y2, x2, y1, x1)
		self.extrude(y1, x1, x1, y1)
		'''
		gl.glEnd()
		gl.glEndList()
		return genList

	def makeObject(self):
		genList = gl.glGenLists(1)
		gl.glNewList(genList, gl.GL_COMPILE)

		gl.glBegin(gl.GL_QUADS)
		#gl.glBegin(gl.GL_TRIANGLES)

		x1 = +0.06
		y1 = -0.14
		x2 = +0.14
		y2 = -0.06
		x3 = +0.08
		y3 = +0.00
		x4 = +0.30
		y4 = +0.22

		self.quad(x1, y1, x2, y2, y2, x2, y1, x1)
		self.quad(x3, y3, x4, y4, y4, x4, y3, x3)

		self.extrude(x1, y1, x2, y2)
		self.extrude(x2, y2, y2, x2)
		self.extrude(y2, x2, y1, x1)
		self.extrude(y1, x1, x1, y1)
		self.extrude(x3, y3, x4, y4)
		self.extrude(x4, y4, y4, x4)
		self.extrude(y4, x4, y3, x3)

		NumSectors = 100

		for i in range(NumSectors):
			angle1 = (i * 2 * math.pi) / NumSectors
			x5 = 0.30 * math.sin(angle1)
			y5 = 0.30 * math.cos(angle1)
			x6 = 0.20 * math.sin(angle1)
			y6 = 0.20 * math.cos(angle1)

			angle2 = ((i + 1) * 2 * math.pi) / NumSectors
			x7 = 0.20 * math.sin(angle2)
			y7 = 0.20 * math.cos(angle2)
			x8 = 0.30 * math.sin(angle2)
			y8 = 0.30 * math.cos(angle2)
			self.quad(x5, y5, x6, y6, x7, y7, x8, y8)
			self.extrude(x6, y6, x7, y7)
			self.extrude(x8, y8, x5, y5)

		gl.glEnd()
		gl.glEndList()
		#print((genList))
		return genList

	def triangle(self, x1, y1, x2, y2, x3, y3):
		z_min = -0.25
		z_max = -0.2
		gl.glVertex3d(x1, y1, z_min)
		gl.glVertex3d(x2, y2, z_min)
		gl.glVertex3d(x3, y3, z_min)
		gl.glVertex3d(x1, y1, z_max)
		gl.glVertex3d(x2, y2, z_max)
		gl.glVertex3d(x3, y3, z_max)

	def quad(self, x1, y1, x2, y2, x3, y3, x4, y4):
		self.setColor(self.trolltechGreen)

		z_min = -0.25
		z_max = -0.2
		gl.glVertex3d(x1, y1, z_min)
		gl.glVertex3d(x2, y2, z_min)
		gl.glVertex3d(x3, y3, z_min)
		gl.glVertex3d(x4, y4, z_min)

		gl.glVertex3d(x4, y4, z_max)
		gl.glVertex3d(x3, y3, z_max)
		gl.glVertex3d(x2, y2, z_max)
		gl.glVertex3d(x1, y1, z_max)

	def extrude(self, x1, y1, x2, y2):
		self.setColor(self.trolltechGreen.darker(250 + int(100 * x1)))

		z_min = -0.25
		z_max = -0.25
		gl.glVertex3d(x1, y1, z_max)
		gl.glVertex3d(x2, y2, z_max)
		gl.glVertex3d(x2, y2, z_min)
		gl.glVertex3d(x1, y1, z_min)

	def normalizeAngle(self, angle):
		while angle < 0:
			angle += 360 * 16
		while angle > 360 * 16:
			angle -= 360 * 16
		return angle

	def setClearColor(self, c):
		gl.glClearColor(c.redF(), c.greenF(), c.blueF(), c.alphaF())

	def setColor(self, c):
		gl.glColor4f(c.redF(), c.greenF(), c.blueF(), c.alphaF())



	def recur_node(self, node,level = 0):
		print("  " + "\t" * level + "- " + str(node))
		for child in node.children:
			recur_node(child, level + 1)


	def print_Meshes(self, scene):
		for index, mesh in enumerate(scene.meshes):
			print("  MESH" + str(index+1))
			print("    material id:" + str(mesh.materialindex+1))
			print("    vertices:" + str(len(mesh.vertices)))
			print("    first 3 verts:\n" + str(mesh.vertices[:3]))
			if mesh.normals.any():
				print("    first 3 normals:\n" + str(mesh.normals[:3]))
			else:
				print("    no normals")
			print("    colors:" + str(len(mesh.colors)))
			tcs = mesh.texturecoords
			if tcs.any():
				for index, tc in enumerate(tcs):
					print("    texture-coords "+ str(index) + ":" + str(len(tcs[index])) + "first3:" + str(tcs[index][:3]))
			else:
				print("    no texture coordinates")
			print("    uv-component-count:" + str(len(mesh.numuvcomponents)))
			print("    faces:" + str(len(mesh.faces)) + " -> first:\n" + str(mesh.faces[:3]))
			print("    bones:" + str(len(mesh.bones)) + " -> first:" + str([str(b) for b in mesh.bones[:3]]))
