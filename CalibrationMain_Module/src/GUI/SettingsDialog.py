"""
HEADER
"""

from PyQt5.QtWidgets import QDialog, QMessageBox
from PyQt5.uic import loadUi
from PyQt5.QtCore import pyqtSlot, QRegExp
from PyQt5.QtGui import QDoubleValidator, QIntValidator, QRegExpValidator

class SettingsDialog(QDialog):
    def  __init__(self, parent, setts):
        super().__init__(parent)
        loadUi('./CalibrationMain_Module/designer/SettingsDialog.ui', self)
        self.show()

        self.settings = setts

        #Variables
        self.dialogStatusList = ["GENERAL","CALIBRACIÓN","MARCADORES"]
        self.dialogStatus = self.dialogStatusList[0]
        self.workSaved = False

        #------Signals------#
        #Tabs
        self.tabWidget.currentChanged.connect(self.onTabChange)
        #Buttons
        self.pushButton_Cancel.clicked.connect(self.slot_pushButton_Cancel_clicked)
        self.pushButton_Aceppt.clicked.connect(self.slot_pushButton_Aceppt_clicked)
        self.pushButton_ResetSettings.clicked.connect(self.slot_pushButton_ResetSettings_clicked)
        self.pushButton_SaveSettings.clicked.connect(self.slot_pushButton_SaveSettings_clicked)
        #Labels
        #self.lineEdit_squareSizemm.setValidator(QDoubleValidator())
        regexp = QRegExp('[2-8]')
        validator = QRegExpValidator(regexp)
        self.lineEdit_heightCorners.setValidator(validator)
        self.lineEdit_WidthCorners.setValidator(validator)

        self.loadParams()
        
    def onTabChange(self, tabID):
        self.dialogStatus = self.dialogStatusList[tabID]

    def loadParams(self):
        self.settings.readSettingsFile()

        self.spinBox_CalibrationFrameNumbers.setValue(self.settings.getCalibrationFrameNumbers())
        self.lineEdit_squareSizeHeight.setText( str(self.settings.getPatternHeightSize()) )
        self.lineEdit_squareSizeWidth.setText( str(self.settings.getPatternWitdhSize()) )

        if self.settings.cam_vec_type[0]==0:
            self.radioButton_Cam0_UVC.setChecked(True)
        else:
            self.radioButton_Cam0_CIP.setChecked(True)
        
        if self.settings.cam_vec_type[1]==0:
            self.radioButton_Cam1_UVC.setChecked(True)
        else:
            self.radioButton_Cam1_CIP.setChecked(True)

        if self.settings.cam_vec_type[2]==0:
            self.radioButton_Cam2_UVC.setChecked(True)
        else:
            self.radioButton_Cam2_CIP.setChecked(True)
        
    @pyqtSlot()
    def slot_pushButton_SaveSettings_clicked(self):

        self.settings.setCalibrationFrameNumbers(
            self.spinBox_CalibrationFrameNumbers.value())

        self.settings.setPatternSquareSize( float( self.lineEdit_squareSizeHeight.text()),
                                            float( self.lineEdit_squareSizeWidth.text()) )


        cam_vec_type = [0, 0, 0]
        if self.radioButton_Cam0_CIP.isChecked():
            cam_vec_type[0]=1
        if self.radioButton_Cam1_CIP.isChecked():
            cam_vec_type[1]=1
        if self.radioButton_Cam2_CIP.isChecked():
            cam_vec_type[2]=1
        self.settings.setCameraProtocalType(cam_vec_type)
        
        #self.checkBox_PatternType_Chessboard
        #self.checkBox_PatternType_Circles
        #self.lineEdit_heightCorners
        #self.lineEdit_WidthCorners
        #self.lineEdit_markerAXpos
        #self.lineEdit_markerAYpos
        
        self.settings.writeSettingsFile()
        self.workSaved = True

    @pyqtSlot()
    def slot_pushButton_Cancel_clicked(self):
        if not self.workSaved:
            buttonReply = QMessageBox.question(self, 
                                               'ALERTA', "¿Salir sin guardar?",
                                               QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
            if buttonReply == QMessageBox.Yes:
                self.closeEvent(None)
        else:
            self.closeEvent(None)
            
    @pyqtSlot()
    def slot_pushButton_Aceppt_clicked(self):
        if not self.workSaved:
            buttonReply = QMessageBox.question(self, 
                                               'ALERTA', "¿Salir sin guardar?",
                                               QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
            if buttonReply == QMessageBox.Yes:
                self.closeEvent(None)
        else:
            self.closeEvent(None)

    @pyqtSlot()
    def slot_pushButton_ResetSettings_clicked(self):
        print("Not implemented yet")

    def closeEvent(self, event):
        QDialog.closeEvent(self, event)

